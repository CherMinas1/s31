const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({ extended:true }));

mongoose.connect('mongodb+srv://cminas:cminas@cluster0.yv1bj.mongodb.net/batch164_to-do?retryWrites=true&w=majority',
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
//connection is successful message
db.once("open", () => console.log("We're connected to the cloud database"))
app.use(express.json());
app.use(express.urlencoded({ extended: true}));


const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);
app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name}, (err, result) => {
		//If a document was found and the document's name matches the information sent via the client
		if(result != null && result.name == req.body.name){
			return res.send("Complete")
		}else{
			//if no document was found 
			//create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask) => {
					//If there are errors in saving
					if(saveErr){
						return console.error(saveErr)
					} else {
						return res.status(201).send("Complete")
					}
			})
		}
	})
}) 

app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err) {
			return console.log(err);
		

	} else {
		return res.status(200).json({
			dataFromMDB: result
		})
	}
	
})
})














app.listen(port, () => console.log(`Server is running at port ${port}`));
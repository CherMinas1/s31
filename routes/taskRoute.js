const express = require("express");
const router = express.Router();

const TaskController = require('../controllers/taskController');

router.get("/", (req, res) => {
	TaskController.getTask().then(resultFromController => res.send(resultFromController));
})


module.exports = router;